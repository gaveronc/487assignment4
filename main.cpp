/////////////////////
//Cameron Gaveronski
//ENEL 487
//Assignment 4
//2016
/////////////////////
//This program tests the output of a given CRC algorithm
//with a check value, outputing to the user if an error occured.
//Regardless of error, the program continues to execute,
//generating the CRC of the given file "wrnpc11.txt".
/////////////////////
#define ITERATIONS 1000000
#define LONGITERATIONS 100

#include "crc.h"
#include <iostream>
#include <fstream>
#include <stdint.h>
#include <time.h>

using namespace std;

int main() {
	string message = "123456789";//Test input, compare the CRC of this to CHECK_VALUE
	ifstream input;
	crc crcVal;
	clock_t start_t, end_t, total_t;
	
	crcInit();
	
	total_t = 0;
	start_t = clock();//Start timer
	for(int i = 0; i < ITERATIONS; i++) {
		crcVal = crcSlow((unsigned char *)message.c_str(), message.length());
		end_t = clock();//Stop timer
		total_t += end_t - start_t;//Accumulate
		start_t = end_t;//Set start to previous end and do it again
	}
	
	cout << "CRC slow of " << message << " is " << hex << crcVal << endl;
	if (crcVal != CHECK_VALUE) {//Test against check value
		cout << "Error detected in CRC calculation!\n";
	}
	cout << "Time taken to calculate: " << dec << (double(total_t)/ITERATIONS)/CLOCKS_PER_SEC << " seconds" << endl;
	
	total_t = 0;
	start_t = clock();
	for(int i = 0; i < ITERATIONS; i++) {
		crcVal = crcFast((unsigned char *)message.c_str(), message.length());
		end_t = clock();
		total_t += end_t - start_t;
		start_t = end_t;
	}
	
	cout << "CRC fast of " << message << " is " << hex << crcVal << endl;
	if (crcVal != CHECK_VALUE) {
		cout << "Error detected in CRC calculation!\n";
	}
	cout << "Ticks taken to calculate: " << dec << (double(total_t)/ITERATIONS)/CLOCKS_PER_SEC << " seconds" << endl;
	
	message = "";
	input.open("wrnpc11.txt");
	//Read in file
	while(!input.eof()) {
		message += input.get();
	}
	
	total_t = 0;
	start_t = clock();
	for(int i = 0; i < LONGITERATIONS; i++) {//Use LONGITERATIONS instead of ITERATIONS to reduce runtime
		crcVal = crcSlow((unsigned char *)message.c_str(), message.length()-1);//Don't count EOF in CRC
		end_t = clock();
		total_t += end_t - start_t;
		start_t = end_t;
	}
	
	cout << "CRC slow of War and Peace is " << hex << crcVal << endl;
	cout << "Time taken to calculate: " << dec << (double(total_t)/LONGITERATIONS)/CLOCKS_PER_SEC << " seconds" << endl;
	
	total_t = 0;
	start_t = clock();
	for(int i = 0; i < LONGITERATIONS; i++) {
		crcVal = crcFast((unsigned char *)message.c_str(), message.length()-1);
		end_t = clock();
		total_t += end_t - start_t;
		start_t = end_t;
	}
	
	cout << "CRC fast of War and Peace is " << hex << crcVal << endl;
	cout << "Time taken to calculate: " << dec << (double(total_t)/LONGITERATIONS)/CLOCKS_PER_SEC << " seconds" << endl;
	
	input.close();
	
	return 0;
}
